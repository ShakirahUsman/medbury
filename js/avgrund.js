(function($) {
  'use strict';
  $(function() {
    $('#show').avgrund({
      height: 500,
      holderClass: 'custom',
      showClose: true,
      showCloseText: 'x',
      onBlurContainer: '.container-scroller',
      template: '<p class="mt-sm-5">What type of client is this?</p>' +
        '<div>' +
        '<a href="" class="btn btn-outline-danger btn-block mt-sm-4" id="projectorg" data-toggle="modal" data-target="#organisationModal" data-whatever="@getbootstrap">Project</a>' +'</div>'+
        '<div>'+
        '<a href="" class="btn btn-outline-danger btn-block mt-sm-4">Clinic</a>' +
        '</div>'
    });
  })
})(jQuery);